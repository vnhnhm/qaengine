You:

 - Were not using the application.html.erb, every content to render goes through the yield function.
 - Hadn't added the style.css file from the template.
 - Had such strange `imports` in the `users.coffee` file.
 - Hadn't added the `DOMContentLoaded` callback for the JS content on `application.js`.

I've cloned from `ssh-iitmandi/QAengine` as you added.